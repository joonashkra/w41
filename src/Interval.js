import React, { useState } from 'react'

export default function Interval() {


    const [intervalArray, setIntervalArray] = useState([])

    const newInterval = () => { 
        const currentTime = new Date(Date.now())
        setIntervalArray([...intervalArray, formatTime(currentTime)])
    }

    const formatTime = (time) => {
        const hours = time.getHours().toString().padStart(2, '0');
        const minutes = time.getMinutes().toString().padStart(2, '0');
        const seconds = time.getSeconds().toString().padStart(2, '0');
        const milliseconds = time.getMilliseconds().toString().padStart(2, '0').padStart(3, '0');
        return `${hours}:${minutes}:${seconds}-${milliseconds}`;
    }

    return (
        <div style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
            <button onClick={newInterval} style={{fontSize: "36px"}}>Interval time</button>
            <table style={{fontSize: "22px"}}>
                <tbody>
                    {intervalArray.map((interval, i) => (
                        <tr key={i}>
                            <td>{interval}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}
