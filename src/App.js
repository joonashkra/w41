import React from 'react';
import logo from './logo.svg';
import './App.css';
import Interval from './Interval';

function App() {
  return (
    <div className="App">
      <header>w41</header>
      <Interval/>
    </div>
  );
}

export default App;
